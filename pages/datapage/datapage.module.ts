import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageoneComponent } from './pageone/pageone.component';
import {RouterModule, Routes} from '@angular/router';
import {LayoutModule} from '../../layouts/layout.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DatapageComponent } from './datapage.component';
import { PagecreateComponent } from './pagecreate/pagecreate.component';

const routes: Routes = [
  {
    path: '',
    component: PageoneComponent,
  },
  {
    path: 'create',
    component: PagecreateComponent,
  }
];
@NgModule({
  declarations: [PageoneComponent, DatapageComponent, PagecreateComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, ReactiveFormsModule,
  ],
  entryComponents: [],
  bootstrap: [PageoneComponent],

})
export class DatapageModule { }

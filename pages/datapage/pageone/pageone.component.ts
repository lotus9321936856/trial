import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../../services/api.service';
import {User} from '../../../../models/User';

@Component({
  selector: 'app-pageone',
  templateUrl: './pageone.component.html',
  styleUrls: ['./pageone.component.css']
})
export class PageoneComponent implements OnInit {
  arrCase: User[];
  shower: string = ''

  constructor(public apiservice: ApiService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.apiservice.getUserList().subscribe(result => {
        this.arrCase = result as User [];
        this.shower = this.arrCase == [] ? 'No Data Found' : 'yes';
      },
      error => {
        this.shower = 'Something went wrong';
      });
  }
}


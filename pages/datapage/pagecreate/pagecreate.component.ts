import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../../services/api.service';

@Component({
  selector: 'app-pagecreate',
  templateUrl: './pagecreate.component.html',
  styleUrls: ['./pagecreate.component.css']
})
export class PagecreateComponent implements OnInit {
  state: string= "Please Fill all details "
  AddDataForm = new FormGroup({
    name: new FormControl('', Validators.required),
    call_representative: new FormControl(true, Validators.required),
    email: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    website: new FormControl('', Validators.required),
    created_by: new FormControl(1, Validators.required),
    industry: new FormControl(1, Validators.required),
  });

  constructor(private api: ApiService, private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
  }


  onSubmit() {
    console.warn(this.AddDataForm.value);
    this.api.AddProjectData(this.AddDataForm.value).subscribe(result => {
this.state = 'Form Submitted'
      },
      error => {
        this.state = 'Something went wrong'

      });
  }
}

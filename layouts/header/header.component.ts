import {Component, Injectable, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
@Injectable()

export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
goHome(){
  this.router.navigate(['/get-data']);
}
  createPage(){
    this.router.navigate(['/get-data/create']);
  }
}
